euPiteco - web, design e ilustração

# Página principal

# euPiteco - web, design e ilustração

Ser humano e designer gráfico de nascido e residente em [Pindorama](https://pt.wikipedia.org/wiki/Pindorama), atuo com criação de marcas, editorial, cartazes, ilustração, caligrafia e desenvolvimento frontend.

Além disso gosto muito de andar de bicicleta, cultivar alimentos, cultura e softwares livres, escrever e desenhar; tenho muito interesse em projetos relacionados a esses temas.

A seguir estão alguns dos trabalhos que tenho feito, se você tem interesse em saber mais sobre eles, contratar algum serviço meu, tem uma ideia maneira de projeto ou só quer conversar mesmo, entre em [contato](#contato) comigo.

## Projetos

Aqui estão os principais projetos que realizei ou nos quais participei. O meu portifólio e currículo completos você pode acessar [aqui](#portfolio).

### HOCCA
Sistema visual e cartazes

Cartazes desenvolvidos para os eventos da [Horta Orgânica do CCA - UFSC](https://www.instagram.com/hocca.ufsc/) entre 2016 e 2018

### Guia de plantas medicinais de Florianópolis
Projeto editorial e diagramação

Diagramação de uma cartilha de plantas medicinais voltada para profissionais de saúde do SUS Florianópolis

### Literatura em Libras
Desenvolvimento frontend 

Implementação versão digital do livro [Literatura em Libras](http://files.literaturaemlibras.com/Literatura_em_Libras_Rachel_Sutton_Spence.pdf) escrito por Rachel Sutton-Spence

## Contatos#contato
- Mail: eupiteco Ⓐ gmail.com
- GitHub: [@eupiteco](http://github.com/eupiteco)
- GitLab: [@eupiteco](http://gitlab.com/eupiteco)

## [Portifólio / Currículo](portfolio_curriculo.pdf)#portfolio


# Página HOCCA

# Cartazes para a Horta Orgânica do CCA
A HOCCA foi um projeto de extensão organizado dentro do Centro de Ciências Agrárias da UFSC para a produção de alimentos orgânicos em espaços ociosos do campus. Além da produção eram organizados encontros, cursos e oficinas para os quais fiz boa parte dos cartazes. Com o tempo, foi desenvolvido um sistema visual para criar coerência entre os cartazes e agilizar a produção.

(Imagens dos cartazes e quiçá uma genérica com o modelo, pensar a composição)

# Página CPIC

# Guia de plantas medicinais de Florianópolis
Um guia para profissionais de saúde do SUS florianópolis concebido e escrito por integrantes da Comissão de Práticas Integrativas e Complementares, Horto Medicial Didático da UFSC, estudantes, professoras e servidoras da UFSC, além de outras estudiosas do temas. A mim coube criar um projeto editorial que organizasse toda a informação em um formato de bolso, que fosse fácil de transportar e consultar, e a diagramação do conteúdo.

(Prints do guia)

# Página LIT

# Versão digital do livro Literatura em Libras
Literatura em Libras é um livro escrito pela Rachel Sutton-Spence, doutora em Língua de Sinais Britânica pela universidade de Bristol e professora na UFSC desde 2013. O desenho do site foi concebido por oão Paulo de Abreu (abreu.joaopaulo EM gmail.com) e Mariana Dornelles (marianadcdesign EM gmail.com) e implementado por mim e pelo Victor Savas (vicotorsavas EM gmail.com). A solução foi transformar o conteúdo do livro em markdown e usar como base de dados pra um app em [GatsbyJS](https://www.gatsbyjs.com/).

O site pode ser acessado [aqui](http://literaturaemlibras.com/)

(Prints do site)