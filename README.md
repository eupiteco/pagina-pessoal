[pt-BR]
# Página pessoal

Um repositório pra minha página pessoal. A ideia é construir aos poucos, experimentando bastante e documentando o máximo possível o processo aqui.

Vou tentar não usar nenhum tipo de plugin o máximo possível (ReactJS, SASS, jade, handlebars etc.) por dois motivos: 
1) [KISS](https://pt.wikipedia.org/wiki/Princ%C3%ADpio_KISS). Se esse *site* precisar de algo mais que html, css e js puros pra funcionar, provavelmente ele ficou complexo demais.

2) Ddática e aprendizagem, entender melhor os rudimentos antes de partir direto pra plugins, bibliiotecas e pré-processadores.

Mais do que um portifólio de grandes habilidades técnicas, a ideia é ser um projeto de potencial criativo, materialização de ideias excêntricas, autoexpressão e diversão.

---

## Trajetória e planejamento

Um esquema linear de organização, indo do mais básico e cru ao mais complexo. Essa separação também permite entender melhor cada parte e como elas se afetam, facilitando futuras alterações.

### 1. Conteúdo
Todos os textos, imagens e mídias que vão estar na página, em formato `.md` ou `.txt` pra monitorar melhor alterações e facilitar a visualização. Melhor ainda seria conseguir importar de alguma forma esse conteúdo no html mantendo a legibilidade, sem ter que fazer algum tipo de tabela.

### 2. Arquitetura
Como esse conteúdo vai ser organizado: seções, páginas, listas etc. A materialização é uma estrutura de arquivos `.html` linkados entre si.

### 3. Estética
A cara que vai ter, cores, forma, tipografia, elementos gráficos e interativos. Pra isso tem a pasta de referências visuais. A ideia do site é ter mais de um tema, não só claro/escuro mas também temas completamente diferentes pra mesma estrutura. Aí tem que ver se com o tempo crio subpastas dentro dessa pra cada um dos temas ou se vou montando paineis com arranjos e combinações diferentes. A materialização vai ser uma ou mais folhas `.css`

### 4. Interações
Por fim, a parte dinâmica. Interações com o usuário (mudança de tema, elemento que troca o estilo em algum momento) e experimentações estéticas (coisas legais do codepen). A materalização vai ser um punhado de arquivos `.js`

## Notas e "regras" gerais

- Dar preferência a hierarquia HTML > CSS > JS na hora de resolver ou implementar alguma coisa.

---
---


[en]
# Personal page

A repository for my personal page. The ideia is to build step by step, experimenting a lot and registering as much as possible the process here.
