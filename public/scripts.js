var temas = [
  {
    nome: "fancy",
    etiqueta: "¶",
  },
  {
    nome: "tipografico",
    etiqueta: "Aa",
  },
  {
    nome: "code",
    etiqueta: "{ }",
  },
  {
    nome: "zero",
    etiqueta: "🛇",
  },
];
var nomesDeTema = temas.map(t => t.nome);
var globalStyleElement;

function lerBusca() {
  if (!location.search) return
  var paramsArr = location.search.slice(1).split("&");
  var params = paramsArr.reduce(function(acc, cur) {
    var curValues = cur.split("=");
    if (curValues[1]) {
      acc[curValues[0]] = curValues[1];
    } else {
      acc[curValues[0]] = true;
    }
    return acc;
  }, {});
  return params;
}

function setarCookieTema(tema) {
  Cookies.set('tema', tema, {expires: 1, path: "/",});
}
function lerCookieTema() {
  return Cookies.get('tema');
}

function criarSeletorDeTemas() {
  var seletor = document.createElement("div");
  seletor.id = "temas";
  seletor.style.width = "100%";
  seletor.style.display = "flex";
  seletor.style.justifyContent = "center";
  seletor.style.position = "fixed";
  seletor.style.top = "0px";
  seletor.style.left = "50%";
  seletor.style.transform = "translateX(-50%)";
  seletor.style.zIndex = "99";
  return seletor;
}

function criarBotaoSeletor({nome, etiqueta}) {
  var botaoSeletor = document.createElement("button");
  botaoSeletor.classList.add("tema-seletor");
  botaoSeletor.id = "tema-seletor-" + nome;
  botaoSeletor.innerText = etiqueta;
  botaoSeletor.style.cursor = "pointer";
  if (nome === lerCookieTema()) botaoSeletor.classList.add("ativo");
  return botaoSeletor;
}

function setarTema(nome) {
  var botoes = document.querySelectorAll(".tema-seletor");
  setarCookieTema(nome);

  document.body.classList.value = lerCookieTema();

  botoes.forEach(function(botao) {
    if (botao.id === "tema-seletor-" + nome) {
      botao.classList.add("ativo");
    } else {
      botao.classList.remove("ativo");
    }
  });

}

function main () {
  globalStyleElement = document.createElement("style");
  var conteinerSeletor = criarSeletorDeTemas();
  var parametros = lerBusca();

  if (parametros && nomesDeTema.indexOf(parametros['tema']) > -1) {
    setarTema(parametros['tema']);
  }

  if (lerCookieTema()) {
    setarTema(lerCookieTema());
  }

  document.body.appendChild(conteinerSeletor);
  document.body.appendChild(globalStyleElement);

  temas.forEach(function (tema) {
    botaoSeletor = criarBotaoSeletor(tema);
    botaoSeletor.addEventListener('click', function() {
      globalStyleElement.innerText="html * { transition: all ease 0.2s }";
      setarTema(tema.nome);
      setTimeout(function() {
        globalStyleElement.innerText="";
      }, 400);
    });
    conteinerSeletor.appendChild(botaoSeletor);
  });
}

document.addEventListener("DOMContentLoaded", main);
