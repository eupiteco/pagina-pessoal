# Tema 1

## Intensão

Sair da forma convencional de sessões empilhadas com 1 ~ 4 colunas e cards,
heros, ctas, texto/desenho/botão e a hegemonia da landing page. Um sentimento de
chegar em um plano, planeta ou dimensão disconhecida. 

Priorizar o experimental sobre o confiável, mas ainda assim guiar a leitura e o
entendimento da informação.

### Palavras chave
- Impactante
- Direto
- Diferente
- 
## Estética

### Palavras chave
- Construtivismo
- Ilustração
- Contraste
- Sobreposição / Colagem
  
### Tipografia
- Texto: 
- Monospace: 
- Display: daFont -> Marola, Fubber, Aurora Pro

## Referências 